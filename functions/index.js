const functions = require("firebase-functions");

exports.split_headers = functions.https.onRequest((req, res) => {
  const lastDigit = (new Date().getUTCSeconds()) % 10;
  if ((lastDigit % 2) == 0) {
    res.send(`<!doctype html>
              <head>
                  <title>Variant A</title>
              </head>
              <body>
                  ${JSON.stringify(req.headers)}
              </body>
          </html>`);
  } else {
    res.send(`<!doctype html>
              <head>
                  <title>Variant B</title>
              </head>
              <body>
                  ${JSON.stringify(req.headers)}
              </body>
          </html>`);
  }
});

exports.split_redirect = functions.https.onRequest((req, res) => {
  const lastDigit = (new Date().getUTCSeconds()) % 10;
  if ((lastDigit % 2) == 0) {
    res.redirect("/about");
  } else {
    res.redirect("/download");
  }
});

exports.split_test = functions.https.onRequest((req, res) => {
  const lastDigit = (new Date().getUTCSeconds()) % 10;
  if ((lastDigit == 0) && (req.headers["x-country-code"] == "US")) {
    res.redirect("/about");
  } else if ((lastDigit == 1) && (req.headers["x-country-code"] == "US")) {
    res.redirect("/download");
  } else {
    res.send(`<!doctype html>
      <head>
        <title>Variant None</title>
      </head>
      <body>
        <h2>Not the variant you're looking for...</h2>
        <h3> Current <i>date>time>second</i> does NOT end with 0 or 1</h3>
        Country code: ${req.headers["x-country-code"]}<br>
        Request original URL: ${req.originalUrl}<br>
        Request path: ${req.path}<br>
        Request params: ${JSON.stringify(req.params)}<br>
      </body>
    </html>`);
  }
});
